package com.db.eurekaaservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer

public class EurekaaserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(EurekaaserviceApplication.class, args);
	}

}
